import styled from 'styled-components'

export const ColContainer = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 100vh;
  .column-first {
    width: 200px;
    min-width: 200px;
    display: flex;
    flex-direction: column;
  }
  .column-mid {
    width: 100%;
    min-width: 100px;
    display: flex;
    flex-direction: column;
  }
  .column-last {
    width: 300px;
    min-width: 300px;
    display: flex;
    flex-direction: column;
  }
  .select-container {
    width: 20%;
  }

  @media (max-width: 600px) {
    .column-last {
      overflow: scroll;
    }
    .column-first {
      overflow: scroll;
    }
    .column-mid {
      overflow: scroll;
    }
  }
`
